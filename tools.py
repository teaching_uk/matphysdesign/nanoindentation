import matplotlib.pyplot as plt
import numpy as np


def plot_dbscan(dbscan, data) :
    """create a plot of the output of the DB Scan algorithm, showing clusters and noise separately.
    Code taken from scikit-learn: https://scikit-learn.org/stable/auto_examples/cluster/plot_dbscan.html#plot-results
    with minor adaptions


    Args:
        dbscan (_type_): _description_
        data (_type_): _description_
    """
    labels = dbscan.labels_
    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)
    
    unique_labels = set(labels)
    core_samples_mask = np.zeros_like(labels, dtype=bool)
    core_samples_mask[dbscan.core_sample_indices_] = True

    colors = [plt.cm.Spectral(each) for each in np.linspace(0, 1, len(unique_labels))]
    for k, col in zip(unique_labels, colors):
        if k == -1:
            # Black used for noise.
            col = [0, 0, 0, 1]

        class_member_mask = labels == k

        xy = data[class_member_mask & core_samples_mask]
        plt.plot(
            xy[:, 0],
            xy[:, 1],
            "o",
            markerfacecolor=tuple(col),
            markeredgecolor="k",
            markersize=5,
        )

        xy = data[class_member_mask & ~core_samples_mask]
        plt.plot(
            xy[:, 0],
            xy[:, 1],
            "o",
            markerfacecolor=tuple(col),
            markeredgecolor="k",
            markersize=3,
        )

    plt.title(f"DBSCAN: Estimated number of clusters: {n_clusters_}")
    plt.tight_layout()
    plt.show()