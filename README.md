# NanoIndentation

## Overview

Nanoindentation stands as a contemporary method within materials science, offering a meticulous approach to characterizing mechanical properties on the nanoscale. This technique involves the controlled application of force through a sharp tip, typically with a nanometer-scale radius, and the subsequent measurement of material response. By observing the depth of penetration, nanoindentation provides valuable data on hardness, elastic modulus, and other mechanical parameters for materials at the micro- and nanoscale. Its application extends across a spectrum of materials, including metals, ceramics, polymers, and biological tissues. This method is instrumental in gaining insights into the nuanced mechanical behavior of materials, aiding researchers and engineers in the precise tailoring of materials for enhanced performance and durability across diverse applications.

In this exercise, we will use pre-recorded nanoindentation data on dual-phase DP800 steel and examine the characteristics of the Martensite and ferrite phase using machine learning.


Note: 
Project dependencies are manged with [Poetry](https://python-poetry.org/)



### Credit
Images and nanoindentation data are provided by M. Wollenweber (IMM)